import {bootstrapApplication} from "@angular/platform-browser";
import {AppComponent} from "./app/app.component";
import {provideConfiguration} from "./app/core/configuration";
import {provideAnimations} from "@angular/platform-browser/animations";
import {provideRouter} from "@angular/router";
import {routes} from "./app/routes";
import {provideHttpClient, withInterceptors} from "@angular/common/http";
import {provideAuthenticationCore} from "./app/features/authentication/core";
import {provideNotifications} from "./app/core/notifications";
import {TokenInterceptor} from "./app/features/authentication/core";

bootstrapApplication(AppComponent, {
  providers: [
    provideConfiguration(),
    provideAnimations(),
    provideHttpClient(
      withInterceptors([TokenInterceptor])
    ),
    provideRouter(routes),
    provideAuthenticationCore(),
    provideNotifications()
  ]
}).catch(err => console.error(err))
