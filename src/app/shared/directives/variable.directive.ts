import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

interface Context {
  $implicit: unknown;
  appVar: unknown;
}

@Directive({
  standalone: true,
  selector: '[appVar]',
})
export class VariableDirective {
  private _isInitialized = false;

  @Input()
  set appVar(value: unknown | null | undefined) {
    this.context.$implicit = value;
    this.context.appVar = value;

    if (!this._isInitialized) {
      this.updateView();
      this._isInitialized = true;
    } else {
      this._vcRef.get(0)?.detectChanges();
    }
  }

  context: Context = {} as Context;

  constructor(
    private _vcRef: ViewContainerRef,
    private _templateRef: TemplateRef<unknown>,
  ) {}

  updateView(): void {
    this._vcRef.clear();
    this._vcRef.createEmbeddedView(this._templateRef, this.context);
  }
}
