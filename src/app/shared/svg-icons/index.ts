import {BasicSvgIcon} from "./svg-icons.enum";
import {tree} from "./assets/tree";
import {docs} from "./assets/docs";
import {services} from "./assets/services";
import {forEach} from "lodash";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {finances} from "./assets/finances";
import {company} from "./assets/company";
import {sections} from "./assets/sections";
import {edx} from "./assets/edx";
import {leaf} from "./assets/leaf";

export const svgIcons = {
  [BasicSvgIcon.Services]: services,
  [BasicSvgIcon.Finances]: finances,
  [BasicSvgIcon.Company]: company,
  [BasicSvgIcon.Sections]: sections,
  [BasicSvgIcon.Documents]: docs,
  [BasicSvgIcon.EDX]: edx,
  [BasicSvgIcon.Leaf]: leaf,
  [BasicSvgIcon.Tree]: tree,
}

export function registerCustomSvgIcons(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
  forEach(
    { ...svgIcons },
    (svgUrl, iconName) => {
      iconRegistry.addSvgIconLiteral(
        iconName,
        sanitizer.bypassSecurityTrustHtml(svgUrl),
      );
    },
  );
}
