export const edx = `
<svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="fill: none;">
    <path d="M16 6H4V22H16V6Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path>
    <path d="M16 18H20V2H8V6" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path>
  </svg>
`;
