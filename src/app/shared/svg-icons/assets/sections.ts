export const sections = `
<svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="fill: none;"><rect width="6" height="6" rx="1" transform="matrix(1 8.74228e-08 8.74228e-08 -1 4 20)" stroke="currentColor"></rect><rect width="6" height="6" rx="1" transform="matrix(1 8.74228e-08 8.74228e-08 -1 14 20)" stroke="currentColor"></rect><rect width="6" height="6" rx="1" transform="matrix(1 8.74228e-08 8.74228e-08 -1 4 10)" stroke="currentColor"></rect><path d="M14 7L20 7M17 10L17 4" stroke="currentColor" stroke-linecap="round"></path></svg>
`;
