export const services = `
<svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="fill: none;">
    <path d="M18 8V4C18 3.44772 17.5523 3 17 3H5C4.44772 3 4 3.44772 4 4V20C4 20.5523 4.44772 21 5 21H17C17.5523 21 18 20.5523 18 20V15.1176" stroke="currentColor"></path>
    <path d="M6.97705 7L13.9231 7" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path>
    <path d="M6.97705 11H11.9385" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path>
    <path d="M14.2198 13.7005L20.8076 4L24.1458 6.04275L17.5581 15.7432L14.669 16.5182L14.2198 13.7005Z" stroke="currentColor" stroke-linejoin="round"></path>
    <path d="M19.334 7.1665L21.5881 8.54585M15.9181 12.1964L18.1722 13.5757" stroke="currentColor"></path>
    <path d="M6.8877 16.8262H8.37614L11 14.5V18.5L14.5 16.5" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path>
</svg>
`;
