export enum BasicSvgIcon {
  Services = 'rlt-services',
  Finances = 'rlt-finances',
  Company = 'rlt-company',
  Sections = 'rlt-sections',
  Documents = 'rlt-documents',
  EDX = 'rlt-edx',
  Tree = 'rlt-tree',
  Leaf = 'rlt-leaf',
}
