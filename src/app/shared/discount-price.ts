export function getDiscountPrice(discount: number): number | undefined {
  if (discount === 5) {
    return 30
  } else if (discount === 10) {
    return 70
  } else if (discount === 15) {
    return 90
  }
  return;
}
