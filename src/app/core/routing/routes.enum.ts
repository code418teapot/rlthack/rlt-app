export enum ApplicationRoutes {
  Authentication = 'authentication',
  Sections = 'sections',
  Services = 'services',
  Company = 'company',
  Achievements = 'achievements',
  Trees = 'trees',
  Docs = 'docs',
  EDX = 'edx',
  Profile = 'profile'
}
