import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "../header/header.component";
import {MatSidenav, MatSidenavModule} from "@angular/material/sidenav";
import {Router, RouterLink, RouterOutlet} from "@angular/router";
import {isNil} from "lodash";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {ApplicationRoutes} from "../../../routing";
import {Store} from "@ngrx/store";
import {signOut} from "../../../../features/authentication/store/actions";
import {AuthenticationRoutes} from "../../../../features/authentication/routing/routes.enum";

@Component({
  selector: 'app-user-sidenav',
  standalone: true,
  imports: [CommonModule, HeaderComponent, MatSidenavModule, RouterOutlet, MatDividerModule, MatIconModule, MatListModule, RouterLink],
  templateUrl: './user-sidenav.component.html',
  styleUrls: ['./user-sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserSidenavComponent {
  @ViewChild("sidenavUser") sidenavUser!: MatSidenav

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _globalStore: Store,
    private _router: Router,
  ) {
  }

  open() {
    this.sidenavUser.open();
    this._changeDetectorRef.markForCheck();
  }

  signOut() {
    this._globalStore.dispatch(signOut());
    this._router.navigate(['/', ApplicationRoutes.Authentication, AuthenticationRoutes.SignIn])
  }

  protected readonly isNil = isNil;
  protected readonly ApplicationRoutes = ApplicationRoutes;
}
