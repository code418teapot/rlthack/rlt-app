import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatToolbarModule} from "@angular/material/toolbar";
import {Store} from "@ngrx/store";
import {userProfileSelector} from "../../../../features/user/store/selectors";
import {VariableDirective} from "../../../../shared/directives";
import {isNil} from 'lodash';
import {MatIconModule} from "@angular/material/icon";
import {BasicSvgIcon} from "../../../../shared/svg-icons/svg-icons.enum";

@Component({
  standalone: true,
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatToolbarModule, VariableDirective, MatIconModule],
})
export class HeaderComponent {
  @Output()
  userInfoClick = new EventEmitter<void>();

  readonly isNil = isNil;

  readonly userProfile$ = this._globalStore.select(userProfileSelector);

  constructor(private _globalStore: Store) {
  }

  getInitials(firstName: string, lastName: string) {
    return firstName[0].toUpperCase() + lastName[0].toUpperCase();
  }

  protected readonly BasicSvgIcon = BasicSvgIcon;
}
