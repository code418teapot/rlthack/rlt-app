import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from "../header/header.component";
import {MatSidenavModule} from "@angular/material/sidenav";
import {RouterOutlet} from "@angular/router";
import {SideNavComponent} from "../side-nav/side-nav.component";

@Component({
  selector: 'app-main-sidenav',
  standalone: true,
  imports: [CommonModule, HeaderComponent, MatSidenavModule, RouterOutlet, SideNavComponent],
  templateUrl: './main-sidenav.component.html',
  styleUrls: ['./main-sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSidenavComponent {

}
