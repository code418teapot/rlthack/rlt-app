import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogoComponent} from "../logo/logo.component";
import {MatListModule} from "@angular/material/list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {RouterLink} from "@angular/router";
import {NavItems} from "../../types";
import {ApplicationRoutes} from "../../../routing";
import {MatIconModule} from "@angular/material/icon";
import {isNil} from "lodash";
import {BasicSvgIcon} from "../../../../shared/svg-icons/svg-icons.enum";
import {Store} from "@ngrx/store";
import {isEdxEnabledSelector} from "../../../../features/user/store/selectors";
import {Observable, startWith} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-side-nav',
  standalone: true,
  imports: [CommonModule, LogoComponent, MatListModule, MatToolbarModule, RouterLink, MatIconModule],
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideNavComponent {
  readonly isEdxEnabled$ = this._globalStore.select(isEdxEnabledSelector);

  readonly navItems$: Observable<NavItems> = this.isEdxEnabled$.pipe(
    startWith([]),
    map(isEdxEnabled => {
        return [
          {
            type: 'link',
            title: 'Услуги',
            link: ['/', ApplicationRoutes.Services],
            icon: BasicSvgIcon.Services
          },
          {
            type: 'link',
            title: 'Финансы',
            link: ['/', ApplicationRoutes.Company],
            icon: BasicSvgIcon.Finances
          },
          {
            type: 'link',
            title: 'Компания',
            link: ['/', ApplicationRoutes.Company],
            icon: BasicSvgIcon.Company
          },
          {
            type: 'link',
            title: 'Секции и сервисы',
            link: ['/', ApplicationRoutes.Sections],
            icon: BasicSvgIcon.Sections
          },
          {
            type: 'link',
            title: 'Документы',
            link: ['/', ApplicationRoutes.Docs],
            icon: BasicSvgIcon.Documents
          },
          isEdxEnabled ? {
            type: 'link',
            title: 'ЭДО',
            link: ['/', ApplicationRoutes.EDX],
            icon: BasicSvgIcon.EDX
          } : null,
          {
            type: 'divider'
          },
          {
            type: 'link',
            title: 'Посади дерево',
            link: ['/', ApplicationRoutes.Trees],
            icon: BasicSvgIcon.Leaf
          },
        ];
      }
    )
  )

  readonly isNil = isNil;

  constructor(private _globalStore: Store) {
  }
}
