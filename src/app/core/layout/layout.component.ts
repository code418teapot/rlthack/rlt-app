import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterLink, RouterOutlet} from "@angular/router";
import {MatSidenavModule} from "@angular/material/sidenav";
import {HeaderComponent} from "./components/header/header.component";
import {LogoComponent} from "./components/logo/logo.component";
import {SideNavComponent} from "./components/side-nav/side-nav.component";
import {Store} from "@ngrx/store";
import {getUserProfile} from "../../features/user/store/actions";
import {MatIconModule} from "@angular/material/icon";
import {MainSidenavComponent} from "./components/main-sidenav/main-sidenav.component";
import {UserSidenavComponent} from "./components/user-sidenav/user-sidenav.component";

@Component({
  standalone: true,
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterOutlet,
    RouterLink,
    MatSidenavModule,
    HeaderComponent,
    LogoComponent,
    SideNavComponent,
    MatIconModule,
    MainSidenavComponent,
    UserSidenavComponent
  ],
})
export class LayoutComponent implements OnInit {
  constructor(
    private _globalStore: Store,
  ) {
  }

  ngOnInit(): void {
    this._globalStore.dispatch(getUserProfile())
  }
}
