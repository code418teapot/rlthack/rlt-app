export interface NavItemDivider {
  type: 'divider'
}

export interface NavItemLink {
  type: 'link',
  title: string;
  link: string[];
  icon?: string;
}

export type NavItems = (NavItemLink | NavItemDivider | null)[]
