import {APP_INITIALIZER, Provider} from '@angular/core';
import {ConfigurationService} from "./configuration.service";

export function loadConfiguration(configurationService: ConfigurationService) {
  return () => configurationService.load();
}

export function provideConfiguration(): Provider[] {
  return [
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfiguration,
      deps: [ConfigurationService],
      multi: true
    }
  ]
}
