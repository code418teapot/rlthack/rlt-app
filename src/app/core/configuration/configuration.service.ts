import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { lastValueFrom } from "rxjs";
import { AppConfiguration } from "../../shared";

const configurationFilePath = 'assets/configuration.json';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  private configuration: AppConfiguration | null = null;

  constructor(private _httpClient: HttpClient) {
  }

  getConfiguration(): AppConfiguration {
    if (this.configuration === null) {
      throw new Error("Configuration is not loaded");

    }

    return this.configuration;
  }

  async load(): Promise<void> {
    this.configuration = await lastValueFrom(
      this._httpClient.get<AppConfiguration>(configurationFilePath)
    );
  }
}
