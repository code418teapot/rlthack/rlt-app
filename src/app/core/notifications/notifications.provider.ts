import {importProvidersFrom} from "@angular/core";
import {MatSnackBarModule} from "@angular/material/snack-bar";

export function provideNotifications() {
  return [
    importProvidersFrom(MatSnackBarModule)
  ]
}
