import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NotificationComponent} from "./components/notification/notification.component";
import {NotificationData} from "./components/types";
import {isNil} from "lodash";

const durationInSeconds = 5;

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private _matSnackBar: MatSnackBar) {
  }

  showSuccess(data: NotificationData, duration?: number) {
    return this._openSnackbar(data, 'success', duration);
  }

  showError(data: NotificationData, duration?: number) {
    return this._openSnackbar(data, 'error', duration);
  }

  private _openSnackbar(data: NotificationData, className: string, duration?: number) {
    return this._matSnackBar.openFromComponent(NotificationComponent, {
      data: data,
      duration: (!isNil(duration) ? duration : durationInSeconds) * 1000,
      panelClass: [className],
      horizontalPosition: "right",
      verticalPosition: "bottom",
    });
  }
}
