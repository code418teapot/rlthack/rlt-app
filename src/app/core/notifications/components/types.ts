export interface NotificationData {
  title?: string;
  message: string;
  urlTitle?: string;
  urlLink?: string | string[];
}
