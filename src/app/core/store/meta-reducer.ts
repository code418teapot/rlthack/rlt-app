import {ActionReducer, INIT, MetaReducer} from '@ngrx/store';

const storeStorageKey = 'state';

export function getInitialState<T>(storage: Storage): T | undefined {
  try {
    return JSON.parse(storage.getItem(storeStorageKey) ?? '') as T;
  } catch {
    return undefined;
  }
}

export function saveStateInStorage(
  storage: Storage
): (reducer: ActionReducer<unknown>) => ActionReducer<unknown> {
  return (reducer: ActionReducer<unknown>): ActionReducer<unknown> => {
    return (state, action) => {
      if (action.type === INIT) {
        const preservedState = getInitialState<unknown>(storage);

        return reducer(preservedState ?? state, action);
      }

      const newState = reducer(state, action);

      storage.setItem(storeStorageKey, JSON.stringify(newState));

      return newState;
    };
  };
}

export const metaReducers: MetaReducer[] = [saveStateInStorage(localStorage)];

