import {Component} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {MatIconModule, MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {registerCustomSvgIcons} from "./shared/svg-icons";

@Component({
  standalone: true,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    RouterOutlet,
    MatIconModule
  ]
})
export class AppComponent {
  constructor(
    private _matIconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer
  ) {
    registerCustomSvgIcons(this._matIconRegistry, this._domSanitizer);
  }
}
