import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {buildApi} from "../api";
import {Observable} from "rxjs";
import {CompanyInfoDto} from "../dtos";

@Injectable({
  providedIn: 'root'
})
export class CompanyDataAccessService {
  private _api = buildApi(this._configuration.getConfiguration());

  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {}

  getMyCompany(): Observable<CompanyInfoDto | undefined> {
    return this._httpClient
      .get<CompanyInfoDto | undefined>(this._api.company.getMyCompany());
  }

  joinToCompany(companyId: number): Observable<unknown> {
    return this._httpClient
      .post(this._api.company.joinToCompany(companyId), {})
  }

  plantTree(): Observable<unknown> {
    return this._httpClient
      .post(this._api.company.plantTree(), {})
  }

  buySomething(price: number, discount: number, name: string) {
    return this._httpClient
      .put(this._api.company.buySomething(), {sum: price, sale: discount, name: name})
  }
}
