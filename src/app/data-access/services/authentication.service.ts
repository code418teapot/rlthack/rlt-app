import {Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {CreateProfileDto, SignInRequestDto} from "../dtos";
import {Observable} from "rxjs";
import {TokensDto} from "../dtos/authentication/tokens.dto";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationDataAccessService {
  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {}

  get apiDefinition() {
    return buildApi(this._configuration.getConfiguration());
  }

  createProfile(createProfileDto: CreateProfileDto): Observable<unknown> {
    return this._httpClient
      .post<unknown>(this.apiDefinition.authentication.createProfile(), createProfileDto);
  }

  signIn(signInRequestDto: SignInRequestDto): Observable<TokensDto> {
    return this._httpClient
      .post<TokensDto>(this.apiDefinition.authentication.signIn(), signInRequestDto);
  }
}
