import {Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {Observable} from "rxjs";
import {UserProfileDto} from "../dtos";

@Injectable({
  providedIn: 'root'
})
export class UsersDataAccessService {
  private _api = buildApi(this._configuration.getConfiguration());

  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {}

  getUserProfile(): Observable<UserProfileDto> {
    return this._httpClient
      .get<UserProfileDto>(this._api.users.getUserProfile());
  }

  getUserTelegramCode() : Observable<string> {
    return this._httpClient
      .get(this._api.users.getUsersTelegramCode(), {
        responseType: 'text'
      })
  }
}
