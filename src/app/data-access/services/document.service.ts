import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {buildApi} from "../api";

@Injectable({
  providedIn: 'root'
})
export class DocumentDataAccessService {

  private _api = buildApi(this._configuration.getConfiguration());

  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {
  }

  signDocument(documentUid: string): Observable<unknown> {
    return this._httpClient.post(this._api.documents.signDocument(documentUid), {})
  }
}
