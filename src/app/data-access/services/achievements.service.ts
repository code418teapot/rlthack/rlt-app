import {Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {Observable} from "rxjs";
import {AchievementDto} from "../dtos/company/achievement.dto";

@Injectable({
  providedIn: 'root'
})
export class AchievementsDataAccessService {
  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {}

  get apiDefinition() {
    return buildApi(this._configuration.getConfiguration());
  }

  getAllAchievements(): Observable<AchievementDto[]> {
    return this._httpClient
      .get<AchievementDto[]>(this.apiDefinition.achievements.getAllAchievements());
  }

  createAchievement(achievement: string): Observable<unknown> {
    let params = new HttpParams();
    params = params.append('achievementType', achievement);

    return this._httpClient
      .put<unknown>(this.apiDefinition.achievements.createAchievement(), null, { params });
  }
}
