import {Injectable} from "@angular/core";
import {buildApi} from "../api";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../core/configuration";
import {Observable} from "rxjs";
import {TreeRegionDto} from "../dtos/tree/region.dto";

@Injectable({
  providedIn: 'root'
})
export class TreesDataAccessService {
  private _api = buildApi(this._configuration.getConfiguration());

  constructor(
    private _httpClient: HttpClient,
    private _configuration: ConfigurationService
  ) {
  }

  getTreeRegionData(): Observable<TreeRegionDto> {
    return this._httpClient
      .get<TreeRegionDto>(this._api.tree.getRegionData());
  }
}
