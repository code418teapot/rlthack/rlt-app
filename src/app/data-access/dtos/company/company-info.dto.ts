import {AchievementDto} from "./achievement.dto";
import {CompanyDocumentDto} from "./document.dto";

export interface CompanyInfoDto {
  id: number;
  title: string;
  fullTitle: string;
  type: string;
  createdDate: string;
  bonusCount: number;
  achievements: AchievementDto[];
  documents: CompanyDocumentDto[];
  treesCount: number;
}
