import {AchievementType} from "../../../features/company/achievement-list/achievment/types/achievement-type";

export interface AchievementDto {
  type: AchievementType,
  bonus: number
}
