export type TreeRegionDto = {
  [key: string]: number
}
