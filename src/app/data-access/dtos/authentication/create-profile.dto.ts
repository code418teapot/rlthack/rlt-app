export interface CreateProfileDto {
  lastName: string;
  firstName: string;
  middleName?: string | null;
  email: string;
  inn: string;
  password: string;
  inviteUid?: string | null;
  region: string;
}

