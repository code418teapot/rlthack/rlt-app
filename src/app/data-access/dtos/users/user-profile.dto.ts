export interface UserProfileDto {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
  inn: string;
  inviteUid: string;
  invitedBy?: number;
}
