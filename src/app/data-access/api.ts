import {AppConfiguration} from "../shared";

const publicPath = 'public';
const protectedPath = 'protected';

const authPath = 'auth';
const companiesPath = 'companies';
const usersPath = 'users';
const achievementsPath = 'achievements'
const documentsPath = 'documents'

export function buildApi({apiUrl}: AppConfiguration) {
  return {
    authentication: {
      createProfile: () => `${apiUrl}/${publicPath}/${usersPath}/register`,
      signIn: () => `${apiUrl}/${publicPath}/${authPath}/login`,
    },
    company: {
      getMyCompany: () => `${apiUrl}/${protectedPath}/${companiesPath}/my-сompany`,
      joinToCompany: (companyId: number) => `${apiUrl}/${protectedPath}/${companiesPath}/join/${companyId}`,
      plantTree: () => `${apiUrl}/${protectedPath}/${companiesPath}/trees/plant`,
      buySomething: () => `${apiUrl}/${protectedPath}/${companiesPath}/buySomething`
    },
    tree: {
      getRegionData: () => `${apiUrl}/${protectedPath}/tree/map`,
    },
    users: {
      getUserProfile: () => `${apiUrl}/${protectedPath}/${usersPath}/profile`,
      getUsersTelegramCode: () => `${apiUrl}/${protectedPath}/${usersPath}/integration/telegram`
    },
    achievements: {
      createAchievement: () => `${apiUrl}/${protectedPath}/${companiesPath}/grab-achievement`,
      getAllAchievements: () => `${apiUrl}/${publicPath}/${achievementsPath}`,
    },
    documents: {
      signDocument: (documentUid: string) => `${apiUrl}/${protectedPath}/${documentsPath}/sign?uid=${documentUid}`
    }
  };
}

