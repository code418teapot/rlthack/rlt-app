import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {ServiceDefinition} from "./types";
import {MatButtonModule} from "@angular/material/button";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {EdxInfoDialogComponent} from "./components/edx-info-dialog/edx-info-dialog.component";
import {Store} from "@ngrx/store";
import {bonusCountSelector, isEdxEnabledSelector} from "../user/store/selectors";
import {combineLatest, Observable, startWith} from "rxjs";
import {map} from "rxjs/operators";
import {isNil} from "lodash";

@Component({
  selector: 'app-services',
  standalone: true,
  imports: [CommonModule, NgOptimizedImage, MatButtonModule, MatDialogModule],
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicesComponent {
  readonly isEdxEnabled$ = this._globalStore.select(isEdxEnabledSelector)
    .pipe(
      startWith(false)
    );

  readonly bonusCount$ = this._globalStore.select(bonusCountSelector)
    .pipe(
      startWith(0)
    );

  readonly services$: Observable<ServiceDefinition[]> = combineLatest([
    this.isEdxEnabled$,
    this.bonusCount$,
  ])
    .pipe(
      map(([isEdxEnabled, bonusCount]) => {
        return [
          {
            title: 'Росэлторг. Электронный документооборот',
            description: 'Росэлторг. Электронный документооборот – Юридически значимый электронный документооборот',
            imageUrl: '/assets/images/services/roseltorg_223.svg',
            showModal: true,
            isEnabled: !isEdxEnabled,
            showSale: false,
          },
          {
            title: 'Росэлторг. Корпоративные закупки',
            description: 'Торговая платформа для корпоративных закупок, закупок в рамках 223-ФЗ, включая закупки МСП',
            imageUrl: '/assets/images/services/roseltorg_223.svg',
            showModal: false,
            isEnabled: true,
            showSale: false,
          },
          {
            title: 'Росэлторг. Государственные закупки',
            description: 'Росэлторг. Государственные закупки – Торговая платформа для закупок в рамках Закона № 44-ФЗ',
            imageUrl: '/assets/images/services/roseltorg_223.svg',
            showModal: false,
            isEnabled: true,
            showSale: false,
          },
          {
            title: 'Росэлторг.Бизнес',
            description: 'Росэлторг.Бизнес – Торговая платформа для закупок частного бизнеса',
            imageUrl: '/assets/images/services/roseltorg_223.svg',
            showModal: false,
            isEnabled: true,
            showSale: !isNil(bonusCount) && bonusCount >= 30,
          },
        ]
      })
    )

  constructor(
    public dialog: MatDialog,
    private _globalStore: Store,
  ) {}

  handleButtonClick(service: ServiceDefinition) {
    if (service.showModal) {
      this.dialog.open(EdxInfoDialogComponent);
    }
  }
}
