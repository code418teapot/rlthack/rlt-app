import { ChangeDetectionStrategy, Component } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";

@Component({
  selector: 'app-info-dialog',
  standalone: true,
  imports: [CommonModule, MatDialogModule, MatButtonModule, NgOptimizedImage],
  templateUrl: './edx-dialog.component.html',
  styleUrls: ['./edx-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EdxDialogComponent {

}
