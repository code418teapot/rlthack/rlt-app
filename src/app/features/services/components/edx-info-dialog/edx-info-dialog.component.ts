import { ChangeDetectionStrategy, Component } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {take} from "rxjs";
import {Store} from "@ngrx/store";
import {enableEdx} from "../../../user/store/actions";

@Component({
  selector: 'app-edx-info-dialog',
  standalone: true,
    imports: [CommonModule, MatButtonModule, MatDialogModule, NgOptimizedImage],
  templateUrl: './edx-info-dialog.component.html',
  styleUrls: ['./edx-info-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EdxInfoDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<EdxInfoDialogComponent>,
    public _globalStore: Store,
  ) {
  }

  handleMainButtonClick() {
    this.dialogRef.afterClosed()
      .pipe(take(1))
      .subscribe(
        () => {
          this._globalStore.dispatch(enableEdx());
        }
      )

    this.dialogRef.close();
  }
}
