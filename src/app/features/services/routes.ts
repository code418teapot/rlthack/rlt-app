import {Routes} from "@angular/router";
import {ServicesComponent} from "./services.component";

export const servicesRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ServicesComponent
  }
]
