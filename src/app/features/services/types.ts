export interface ServiceDefinition {
  title: string;
  description: string;
  url?: string;
  imageUrl: string;
  showModal: boolean;
  isEnabled: boolean;
  showSale: boolean;
}
