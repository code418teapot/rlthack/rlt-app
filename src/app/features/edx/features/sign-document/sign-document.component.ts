import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sign-document',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sign-document.component.html',
  styleUrls: ['./sign-document.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignDocumentComponent {

}
