import {Routes} from "@angular/router";
import {EdxComponent} from "./edx.component";
import {SignDocumentComponent} from "./features/sign-document/sign-document.component";

export const edxRoutes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: EdxComponent
  },
  {
    path: 'signDoc',
    component: SignDocumentComponent
  }
]
