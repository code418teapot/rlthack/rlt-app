export interface Document {
  id: number,
  createdDate: string,
  name: string,
  signUid: string,
  signDate: string,
  companyId: number,
  signed: boolean;
}
