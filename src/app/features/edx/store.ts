import {Document} from "./types";
import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {CompanyDataAccessService, DocumentDataAccessService} from "../../data-access";
import {combineLatestWith, EMPTY, finalize, Observable, withLatestFrom} from "rxjs";
import {catchError, switchMap, tap} from "rxjs/operators";
import {AchievementDto} from "../../data-access/dtos/company/achievement.dto";
import {CompanyStore} from "../company/store";
import {find, isNil} from "lodash";
import {AchievementType} from "../company/achievement-list/achievment/types/achievement-type";
import {Store} from "@ngrx/store";
import {createAchievement} from "../user/store/actions";
import {FrontendAchievements} from "../achievements";
import {NotificationService} from "../../core/notifications";

export interface EdxStoreState {
  isLoading: boolean;
  documents: Document[]
}

@Injectable()
export class EdxStore extends ComponentStore<EdxStoreState> {
  readonly documents$ = this.select(state => state.documents);

  constructor(
    private _companyDataAccessService: CompanyDataAccessService,
    private _documentDataAccessService: DocumentDataAccessService,
    private _globalStore: Store,
    private _notification: NotificationService
  ) {
    super({
      isLoading: false,
      documents: []
    });
  }

  readonly fetchMyDocuments = this.effect(
    (trigger$: Observable<void>) => {
      return trigger$.pipe(
        tap(() => this.patchState({
          isLoading: true
        })),
        switchMap(() =>
          this._companyDataAccessService.getMyCompany().pipe(
            finalize(() => {
              this.patchState({
                isLoading: false,
              })
            }),
            tap((company) => {
              this.patchState({
                documents: company?.documents ?? []
              });
            }),
            catchError(() => EMPTY),
          ),
        ),
      );
    },
  );

  signDocument(documentUid: string) {
    this._documentDataAccessService.signDocument(documentUid)
      .pipe(
        combineLatestWith(this._companyDataAccessService.getMyCompany()),
        tap(([signResult, company]) => {
          const signAchievement = find(company?.achievements, achievement =>
            achievement.type === AchievementType.SIGN_DOC_EDO)
          if (isNil(signAchievement)) {
            this._globalStore.dispatch(createAchievement({
              achievement: FrontendAchievements.SignDocEdo
            }))
            this._notification.showSuccess({
              title: 'Новое достижение!',
              message: 'Вы получили 30 бонусов за достижение "Дело в цифре"!'
            })
          }
        }))
      .subscribe({
        next: () => {
          this._notification.showSuccess({
            title: 'Документ подписан',
            message: `Вы успешно подписали документ`
          })
          this.fetchMyDocuments()
        },
        error: (err) => {
          console.error(err)
          this._notification.showError({
            title: 'Подписать документ не удалось!',
            message: 'В процессе подписи документа возникла ошибка'
          })
        }
      })
  }
}

