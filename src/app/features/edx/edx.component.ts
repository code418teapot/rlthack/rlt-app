import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatDividerModule} from "@angular/material/divider";
import {QRCodeModule} from "angularx-qrcode";
import {MatButtonModule} from "@angular/material/button";
import {EdxStore} from "./store";
import {VariableDirective} from "../../shared/directives";
import {MatIconModule} from "@angular/material/icon";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {EdxDialogComponent} from "../services/components/edx-dialog/edx-dialog.component";
import {CompanyStore} from "../company/store";
import {AchievementDto} from "../../data-access/dtos/company/achievement.dto";
import {find, isNil} from "lodash";
import {AchievementType} from "../company/achievement-list/achievment/types/achievement-type";

@Component({
  selector: 'app-edx',
  standalone: true,
  imports: [
    CommonModule,
    MatDividerModule,
    QRCodeModule,
    NgOptimizedImage,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    VariableDirective
  ],
  templateUrl: './edx.component.html',
  styleUrls: ['./edx.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    EdxStore
  ]
})
export class EdxComponent implements OnInit {
  documents$ = this._store.documents$;

  constructor(
    private _store: EdxStore,
    public matDialog: MatDialog,
  ) {}

  signDocument(documentUid: string) {
    this._store.signDocument(documentUid)
  }

  openInfoDialog() {
    this.matDialog.open(EdxDialogComponent);
  }

  ngOnInit() {
    this._store.fetchMyDocuments();
  }

  protected readonly isNil = isNil;
}
