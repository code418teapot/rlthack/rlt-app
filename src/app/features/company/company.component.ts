import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CompanyStore} from "./store";
import {VariableDirective} from "../../shared/directives";
import {AsyncPipe, DatePipe, NgIf} from "@angular/common";
import {isNil} from 'lodash';
import {MatTabsModule} from "@angular/material/tabs";
import {AchievementListComponent} from "./achievement-list/achievement-list.component";
import {FormsModule} from "@angular/forms";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {GettingStartedModalComponent} from "./getting-started-modal/getting-started-modal.component";

@Component({
  standalone: true,
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    VariableDirective,
    AsyncPipe,
    NgIf,
    DatePipe,
    MatTabsModule,
    AchievementListComponent,
    FormsModule,
    MatDialogModule
  ],
  providers: [
    CompanyStore
  ]
})
export class CompanyComponent implements OnInit {
  readonly company$ = this._store.company$;
  readonly isLoading$ = this._store.isLoading$;

  companyId: number = 1
  selectedIndex: number = 0

  readonly isNil = isNil;

  constructor(
    private _store: CompanyStore,
    private _dialog: MatDialog,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (!localStorage.getItem('gettingStartedModalViewed')) {
      this._dialog.open(GettingStartedModalComponent)
        .afterClosed().subscribe(
          value => {
            this.selectedIndex = 2
            this._cd.markForCheck()
            localStorage.setItem('gettingStartedModalViewed', 'true')
          }
      );
    }
  }

  onSelectedIndexChange(index: number) {
    this.selectedIndex = index
  }

  joinCompany() {
    this._store.joinToCompany(this.companyId)
  }
}
