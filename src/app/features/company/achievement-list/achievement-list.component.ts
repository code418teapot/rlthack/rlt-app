import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AchievementComponent} from "./achievment/achievement.component";
import {MatGridListModule} from "@angular/material/grid-list";
import {AchievementDescription} from "./achievment/types/achievement-description";
import {AchievementIcon} from "./achievment/types/achievement-icon";
import {AchievementTitle} from "./achievment/types/achievement-title";
import {CompanyStore} from "../store";
import {AchievementDto} from "../../../data-access/dtos/company/achievement.dto";
import {find, isNil} from "lodash";
import {VariableDirective} from "../../../shared/directives";

@Component({
  selector: 'app-achievement-list',
  standalone: true,
  imports: [CommonModule, AchievementComponent, MatGridListModule, VariableDirective],
  templateUrl: './achievement-list.component.html',
  styleUrls: ['./achievement-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AchievementListComponent {

  protected readonly AchievementDescription = AchievementDescription;
  protected readonly AchievementIcon = AchievementIcon;
  protected readonly AchievementTitle = AchievementTitle;

  readonly allAchievements$  = this._companyStore.allAchievements$;

  readonly companyAchievements$ = this._companyStore.achievements$;

  constructor(
    private _companyStore: CompanyStore,
  ) {}

  getDescription(achievementDto: AchievementDto): AchievementDescription {
    return AchievementDescription[achievementDto.type]
  }

  getTitle(achievementDto: AchievementDto): AchievementTitle {
    return AchievementTitle[achievementDto.type]
  }

  getIcon(achievementDto: AchievementDto): AchievementIcon {
    return AchievementIcon[achievementDto.type]
  }

  isAchievementDisabled(achievement: AchievementDto, companyAchievements: AchievementDto[]): boolean {
    return isNil(find(companyAchievements, (companyAchievement) =>
      companyAchievement?.type === achievement.type))
  }
}
