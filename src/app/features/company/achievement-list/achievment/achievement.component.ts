import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {AchievementIcon} from "./types/achievement-icon";
import {AchievementTitle} from "./types/achievement-title";
import {AchievementDescription} from "./types/achievement-description";

@Component({
  selector: 'app-achievment',
  standalone: true,
  imports: [CommonModule, NgOptimizedImage],
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AchievementComponent {
  @Input() icon: AchievementIcon = AchievementIcon.NO_ICON
  @Input() title: AchievementTitle = AchievementTitle.NO_TEXT
  @Input() description: AchievementDescription = AchievementDescription.NO_DESCRIPTION
  @Input() bonuses: number = 0
  @Input() disabled: boolean = true;
}
