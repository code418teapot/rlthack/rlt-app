export enum AchievementIcon {
  NO_ICON = 'no-image.png',
  FULL_PACKET = 'full-packet.svg',
  IN_STREAM = 'in-stream.svg',
  GETTING_STARTED = 'getting-started.svg',
  FIRST_SUCCESS = 'first-success.svg',
  EXPERIENCED_PLAYER = 'experienced-player.svg',
  FOREST_FRIEND = 'forest-friend.svg',
  SIGN_DOC_EDO = 'about-the-numbers.svg',
  BUY_EDO = 'no-paper.svg'
}
