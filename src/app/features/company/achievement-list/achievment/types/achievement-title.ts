export enum AchievementTitle {
  NO_TEXT = '[НЕТ ТЕКСТА]',
  FULL_PACKET = 'Полный пакет',
  IN_STREAM = 'В потоке',
  GETTING_STARTED = 'Начало положено',
  FIRST_SUCCESS = 'Первый успех',
  EXPERIENCED_PLAYER = 'Опытный игрок',
  FOREST_FRIEND = 'Друг леса',
  SIGN_DOC_EDO = 'Дело в цифре',
  BUY_EDO = 'Никакой бумаги'
}
