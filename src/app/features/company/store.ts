import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {CompanyDataAccessService} from "../../data-access";
import {takeUntil} from "rxjs";
import {AchievementDto} from "../../data-access/dtos/company/achievement.dto";
import {AchievementsDataAccessService} from "../../data-access/services/achievements.service";
import {Store} from "@ngrx/store";
import {getUserProfile} from "../user/store/actions";
import {NotificationService} from "../../core/notifications";
import {getDiscountPrice} from "../../shared/discount-price";

export interface CompanyStoreState {
  isLoading: boolean;
  company?: {
    id: number;
    title: string;
    fullTitle: string;
    type: string;
    createdDate: string;
  }
  achievements: AchievementDto[],
  allAchievements: AchievementDto[],
  treesCount: number
}

@Injectable()
export class CompanyStore extends ComponentStore<CompanyStoreState> {
  readonly isLoading$ = this.select((state) => state.isLoading);
  readonly company$ = this.select(state => state.company);
  readonly achievements$ = this.select(state => state.achievements);
  readonly allAchievements$ = this.select(state => state.allAchievements);
  readonly treesCount$ = this.select(state =>  state.treesCount)

  constructor(
    private _companyDataAccessService: CompanyDataAccessService,
    private _achievementsDataAccessService: AchievementsDataAccessService,
    private _globalState: Store,
    private _notifications: NotificationService
  ) {
    super({
      company: undefined,
      achievements: [],
      allAchievements: [],
      treesCount: 0,
      isLoading: true,
    });

    this.fetchCompanyInformation();
    this._fetchAllAchievements();
  }

  fetchCompanyInformation() {
    this._companyDataAccessService.getMyCompany()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(dto => this.patchState({
        company: dto,
        achievements: dto?.achievements || [],
        treesCount: dto?.treesCount || 0,
        isLoading: false
      }))
  }

  joinToCompany(companyId: number) {
    this._companyDataAccessService.joinToCompany(companyId)
      .subscribe({
        next: () => {
          this.fetchCompanyInformation()
          this._fetchAllAchievements()
        },
        error: console.error
      })
  }

  plantTree() {
    this._companyDataAccessService.plantTree()
      .subscribe({
        next: () => {
          this.fetchCompanyInformation()
          this._globalState.dispatch(getUserProfile())
        },
        error: console.error
      })
  }

  buySomething(price: number, discount: number, name: string) {
    this._companyDataAccessService.buySomething(price, discount, name)
      .subscribe({
        next: () => {
          this.fetchCompanyInformation()
          this._globalState.dispatch(getUserProfile())
          this._notifications.showSuccess({
            title: `Вы успешно купили "${name}"`,
            message: `С Вас снято ${getDiscountPrice(discount) || 0} бонусов`
          })
        },
        error: (value) => {
          console.error(value)
          this._notifications.showError({
            title: 'Не удалось сделать покупку',
            message: value.error.text
          })
        }
      })
  }

  private _fetchAllAchievements() {
    this._achievementsDataAccessService.getAllAchievements()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(dto => this.patchState({
        allAchievements: dto
      }))
  }
}
