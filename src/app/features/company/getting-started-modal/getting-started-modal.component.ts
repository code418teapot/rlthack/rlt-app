import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatTabsModule} from "@angular/material/tabs";
import {MatStepperModule} from "@angular/material/stepper";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {NotificationService} from "../../../core/notifications";
import {take} from "rxjs";
import {Store} from "@ngrx/store";
import {createAchievement} from "../../user/store/actions";
import {FrontendAchievements} from "../../achievements";

@Component({
  selector: 'app-getting-started-modal',
  standalone: true,
  imports: [CommonModule, MatTabsModule, MatStepperModule, NgOptimizedImage, MatButtonModule, MatDialogModule],
  templateUrl: './getting-started-modal.component.html',
  styleUrls: ['./getting-started-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GettingStartedModalComponent {
  constructor(
    public dialogRef: MatDialogRef<GettingStartedModalComponent>,
    private _globalStore: Store,
    private _notificationService: NotificationService,
  ) {
  }

  handleGoClick() {
    this.dialogRef.afterClosed()
      .pipe(
        take(1)
      )
      .subscribe(() => {
        this._notificationService.showSuccess({
          title: 'Вам начислены баллы',
          message: 'Вы получили приветственные 20 баллов за достижение “Добро пожаловать”'
        })
          .afterOpened()
          .pipe(
            take(1),
          )
          .subscribe(() => {
            this._globalStore.dispatch(createAchievement({
              achievement: FrontendAchievements.GettingStarted
            }))
          })
      })

    this.dialogRef.close();
  }
}
