import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {BasicSvgIcon} from "../../shared/svg-icons/svg-icons.enum";
import {CompanyStore} from "../company/store";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {ServiceTile} from "./types";
import {getDiscountPrice} from "../../shared/discount-price";

@Component({
  selector: 'app-company-services',
  standalone: true,
  imports: [CommonModule, MatGridListModule, MatCardModule, MatButtonModule, MatIconModule, MatButtonToggleModule],
  templateUrl: './company-services.component.html',
  styleUrls: ['./company-services.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    CompanyStore
  ]
})
export class CompanyServicesComponent {
  virtualSignatures: ServiceTile[] = [
    {
      title: "Электронная подпись для государственных торгов",
      description: "Сможете участвовать в торгах на 9 федеральных площадках по 44-ФЗ. В дополнение - доступ в ЕИС Закупки, к государственным порталам и системам.",
      price: 10,
      discount: 0,
      discountToggle: false
    },
    {
      title: "Электронная подпись для коммерческих торгов",
      description: "Подойдет для заказчиков и поставщиков по корпоративным закупкам. Сможете учавствовать в торгах на 127 коммерческих площадках 223-ФЗ. В дополнение - доступ к 100+ госпорталам и системам.",
      price: 20,
      discount: 0,
      discountToggle: false
    },
    {
      title: "Электронная подпись для торгов по банкротству",
      description: "Получите доступ к 25 торговым площадкам по банкротству. В дополнение - доступ к 100+ госпорталам и системам.",
      price: 30,
      discount: 0,
      discountToggle: false
    }
  ]

  constructor(private _companyStore: CompanyStore) {
  }

  setDiscount(serviceTile: ServiceTile, discount: number) {
    if (discount === 0) {
      serviceTile.discountToggle = false
    }
    serviceTile.discount = discount
  }

  onBuy(serviceTile: ServiceTile) {
    this._companyStore.buySomething(serviceTile.price, serviceTile.discount, serviceTile.title)
  }

  toggleDiscount(serviceTile: ServiceTile) {
    serviceTile.discountToggle = true
  }

  protected readonly BasicSvgIcon = BasicSvgIcon;
  protected readonly getDiscountPrice = getDiscountPrice;
}
