
export interface ServiceTile {
  title: string,
  description: string,
  price: number,
  discount: number,
  discountToggle: boolean
}
