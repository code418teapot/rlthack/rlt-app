import {Routes} from "@angular/router";
import {CompanyServicesComponent} from "./company-services.component";

export const companyServiceRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CompanyServicesComponent
  }
]
