export enum AuthenticationRoutes {
  CreateProfile = 'create-profile',
  SignIn = 'sign-in',
}
