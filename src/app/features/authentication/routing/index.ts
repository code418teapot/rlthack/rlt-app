import {Routes} from "@angular/router";
import {AuthenticationComponent} from "../authentication.component";
import {AuthenticationRoutes} from "./routes.enum";

export const authenticationRoutes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: AuthenticationRoutes.SignIn,
      },
      {
        path: AuthenticationRoutes.CreateProfile,
        loadChildren: async () =>
          (await import('../features/registration')).registerRoutes,
      },
      {
        path: AuthenticationRoutes.SignIn,
        loadChildren: async () =>
          (await import('../features/login')).loginRoutes,
      },
    ],
  },
];
