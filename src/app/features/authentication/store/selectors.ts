import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthenticationState, featureName } from './types';
import * as dayjs from "dayjs";

export const authSelector =
  createFeatureSelector<AuthenticationState>(featureName);

export const isAuthenticatedSelector = createSelector(
  authSelector,
  (state) => {
    // If we are not authenticated
    if (!state.isAuthenticated) return false;

    // We should also care about token expiration
    return dayjs().isBefore(dayjs(state.tokens.expiresIn));
  }
);
