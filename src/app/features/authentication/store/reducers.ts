import { AuthenticationState } from './types';
import { createReducer, on } from '@ngrx/store';
import { signIn, signInFailure, signInSuccess, signOut } from './actions';
import jwtDecode from 'jwt-decode';

interface AccessTokenData {
  roles: string[]
}

const initialState: AuthenticationState = {
  isAuthenticated: false,
  isLoading: false,
};

export const authenticationReducer = createReducer<AuthenticationState>(
  initialState,
  on(signIn, (state) => ({ ...state, isLoading: true })),
  on(signInSuccess, (state, tokens) => {
    const {
      roles
    } = jwtDecode<AccessTokenData>(tokens.accessToken);

    return {
      ...state,
      tokens,
      isAuthenticated: true,
      isLoading: false,
      isAdmin: roles.includes("ADMIN")
    };
  }),
  on(signInFailure, signOut, (state) => ({ ...state, ...initialState }))
);
