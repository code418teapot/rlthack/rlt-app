import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {SignInStore} from "./store";
import {MatButtonModule} from "@angular/material/button";
import {VariableDirective} from "../../../../shared/directives";
import {MatDividerModule} from "@angular/material/divider";
import {ApplicationRoutes} from "../../../../core/routing";
import {AuthenticationRoutes} from "../../routing/routes.enum";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    VariableDirective,
    MatDividerModule,
    RouterLink
  ],
  providers: [
    SignInStore
  ]
})
export class LoginComponent {
  readonly createProfileLinkOptions = [
    '/',
    ApplicationRoutes.Authentication,
    AuthenticationRoutes.CreateProfile
  ]

  form = new FormGroup({
    login: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
  });

  readonly isLoading$ = this._store.isLoading$;
  readonly hasErrors$ = this._store.hasErrors$;

  constructor(
    private _store: SignInStore
  ) {
  }

  handleFormSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this._store.signIn(this.form.getRawValue())
  }
}
