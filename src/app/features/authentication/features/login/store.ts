import {Injectable} from "@angular/core";
import {ComponentStore} from "@ngrx/component-store";
import {Store as GlobalStore} from '@ngrx/store';
import {Actions, ofType} from "@ngrx/effects";
import {Router} from "@angular/router";
import {Observable, takeUntil} from "rxjs";
import {switchMap, tap} from "rxjs/operators";
import {SignInFormReturnValue} from "./types";
import {signIn, signInFailure, signInSuccess} from "../../store/actions";
import {authSelector} from "../../store/selectors";
import {ApplicationRoutes} from "../../../../core/routing";

interface SignInStoreState {
  isLoading: boolean;
  hasErrors: boolean;
}

@Injectable()
export class SignInStore extends ComponentStore<SignInStoreState> {
  isLoading$ = this.select((state) => state.isLoading);
  hasErrors$ = this.select((state) => state.hasErrors);

  private _companyPageLinkOptions = ['/', ApplicationRoutes.Company];

  constructor(
    private _globalStore: GlobalStore,
    private _actions: Actions,
    private _router: Router
  ) {
    super({
      isLoading: false,
      hasErrors: false,
    });

    this._subscribeStoreAndRouterToSignInSuccessAction();
    this._subscribeStoreToSignInFailureAction();
  }

  readonly signIn = this.effect(
    (signInFormReturnValue$: Observable<SignInFormReturnValue>) => {
      return signInFormReturnValue$.pipe(
        tap((signInFormReturnValue) => {
          this.setState({hasErrors: false, isLoading: true});
          this._globalStore.dispatch(signIn({
            email: signInFormReturnValue.login,
            password: signInFormReturnValue.password
          }));
        })
      );
    }
  );

  private _subscribeStoreAndRouterToSignInSuccessAction(): void {
    this._actions
      .pipe(
        ofType(signInSuccess),
        switchMap(() => this._globalStore.select(authSelector)),
        tap(() => this.patchState({isLoading: false})),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this._router.navigate(this._companyPageLinkOptions);
      });
  }

  private _subscribeStoreToSignInFailureAction(): void {
    this._actions
      .pipe(ofType(signInFailure), takeUntil(this.destroy$))
      .subscribe(() => this.setState({hasErrors: true, isLoading: false}));
  }
}
