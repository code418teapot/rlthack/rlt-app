import {ComponentStore} from "@ngrx/component-store";
import {AuthenticationDataAccessService} from "../../../../data-access";
import {CreateProfileFormValue} from "./types";
import {catchError, EMPTY, finalize, Observable, switchMap, tap, withLatestFrom} from "rxjs";
import {Injectable} from "@angular/core";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {NotificationService} from "../../../../core/notifications";
import {ApplicationRoutes} from "../../../../core/routing";
import {AuthenticationRoutes} from "../../routing/routes.enum";

interface CreateProfileStoreState {
  isLoading: boolean;
  hasErrors: boolean;
  inviteUid?: string | null;
}

const INVITE_UID_QUERY_PARAM = 'inviteUid'

@Injectable()
export class CreateProfileStore extends ComponentStore<CreateProfileStoreState> {
  isLoading$ = this.select((state) => state.isLoading);
  hasErrors$ = this.select((state) => state.hasErrors);
  inviteUid$ = this.select((state) => state.inviteUid);

  constructor(
    private _authenticationDataAccessService: AuthenticationDataAccessService,
    private _router: Router,
    private _notifications: NotificationService,
    private _activatedRoute: ActivatedRoute
  ) {
    super({
      isLoading: false,
      hasErrors: false,
    });
    this.getInviteUid(_activatedRoute.queryParamMap)
  }

  private getInviteUid = this.effect((queryParams$: Observable<ParamMap>) => {
    return queryParams$.pipe(
      tap((params) =>
        this.patchState({inviteUid: params.get(INVITE_UID_QUERY_PARAM)}))
    )
  })

  readonly createProfile = this.effect((createProfile$: Observable<CreateProfileFormValue>) => {
    return createProfile$.pipe(
      tap(() => this.patchState({isLoading: true})),
      withLatestFrom(this.inviteUid$),
      switchMap(([form, inviteUid]) => this._authenticationDataAccessService.createProfile({
          lastName: form.lastName,
          firstName: form.firstName,
          middleName: form.middleName,
          email: form.email,
          inn: form.inn,
          password: form.password,
          inviteUid: inviteUid,
          region: 'RU-TVE'
        }).pipe(
          finalize(() => this.patchState({isLoading: false})),
          tap({
            next: () => {
              this._router.navigate(['/', ApplicationRoutes.Authentication, AuthenticationRoutes.SignIn])
                .then(() => this._notifications.showSuccess({
                  message: "Вы успешно зарегистрированы"
                }));
            },
            error: () => {
              this.patchState({hasErrors: true})
            }
          }),
          catchError(() => EMPTY),
        )),
    );
  });
}
