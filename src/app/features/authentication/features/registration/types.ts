export interface CreateProfileFormValue {
  lastName: string;
  firstName: string;
  middleName?: string | null;
  email: string;
  inn: string
  password: string
}
