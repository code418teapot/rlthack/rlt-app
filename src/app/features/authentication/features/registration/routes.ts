import {Routes} from "@angular/router";
import {RegistrationComponent} from "./registration.component";

export const registerRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RegistrationComponent
  }
]
