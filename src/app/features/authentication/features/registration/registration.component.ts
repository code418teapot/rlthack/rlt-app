import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {VariableDirective} from "../../../../shared/directives";
import {CreateProfileStore} from "./store";
import {ApplicationRoutes} from "../../../../core/routing";
import {AuthenticationRoutes} from "../../routing/routes.enum";
import {RouterLink} from "@angular/router";
import {isNil} from "lodash";


@Component({
  selector: 'app-registration',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MatButtonModule, MatDividerModule, MatFormFieldModule, MatInputModule, VariableDirective, RouterLink],
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    CreateProfileStore
  ]
})
export class RegistrationComponent {
  readonly signInLinkOptions = [
    '/',
    ApplicationRoutes.Authentication,
    AuthenticationRoutes.SignIn
  ]

  form = new FormGroup({
    firstName: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    lastName: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    middleName: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
    email: new FormControl('', {
      nonNullable: true,
      validators: [
        Validators.required,
        Validators.email
      ]
    }),
    inn: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, Validators.minLength(12)]
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required]
    }),
  });

  isLoading$ = this._store.isLoading$;
  hasErrors$ = this._store.hasErrors$;
  inviteUid$ = this._store.inviteUid$;

  constructor(
    private _fb: FormBuilder,
    private _store: CreateProfileStore,
  ) {}

  handleFormSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this._store.createProfile(this.form.getRawValue());
  }

  protected readonly isNil = isNil;
}
