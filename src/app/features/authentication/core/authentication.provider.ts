import {EnvironmentProviders, Provider} from "@angular/core";
import {provideState, provideStore} from "@ngrx/store";
import {metaReducers} from "../../../core/store";
import {provideEffects} from "@ngrx/effects";
import {AuthenticationEffects} from "../store/effects";
import {featureName} from "../store/types";
import {authenticationReducer} from "../store/reducers";
import {AuthenticationGuard} from "./guards";
import {AlreadyAuthenticatedGuard} from "./guards";

export function provideAuthenticationCore(): (Provider | EnvironmentProviders)[] {
  return [
    provideStore({}, {
      metaReducers
    }),
    provideState(featureName, authenticationReducer),
    provideEffects(AuthenticationEffects),
    AuthenticationGuard,
    AlreadyAuthenticatedGuard
  ]
}

