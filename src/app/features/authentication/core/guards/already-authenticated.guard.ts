import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {map, Observable} from "rxjs";
import {isAuthenticatedSelector} from "../../store/selectors";
import {ApplicationRoutes} from "../../../../core/routing";

@Injectable()
export class AlreadyAuthenticatedGuard implements CanActivate {
  constructor(
    private _globalStore: Store,
    private _router: Router,
  ) {}

  canActivate(): Observable<boolean> {
    return this._globalStore.pipe(
      select(isAuthenticatedSelector),
      map((isAuthenticated) => {
        if (isAuthenticated) {
          this._router.navigate(['/', ApplicationRoutes.Company]);
        }

        return true;
      })
    );
  }
}
