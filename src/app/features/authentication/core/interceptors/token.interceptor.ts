import {HttpHandlerFn, HttpRequest} from '@angular/common/http';
import {inject} from '@angular/core';
import {first, Observable, switchMap} from 'rxjs';
import {Store} from '@ngrx/store';
import {AuthenticatedState} from "../../store/types";
import {authSelector} from "../../store/selectors";

export function TokenInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn){
  const globalStore = inject(Store);

  const authState$ = globalStore
    .select(authSelector)
    .pipe(first()) as Observable<AuthenticatedState>;

  return authState$.pipe(
    switchMap((auth) => {
      if (!auth.isAuthenticated) {
        return next(req);
      }

      const { accessToken } = auth.tokens;

      const authenticatedRequest = accessToken
        ? req.clone({
          setHeaders: { Authorization: `Bearer ${accessToken}` },
        })
        : req;

      return next(authenticatedRequest);
    }),
  );
}
