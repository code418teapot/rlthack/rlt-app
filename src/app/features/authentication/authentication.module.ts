import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import {RouterModule} from "@angular/router";
import {authenticationRoutes} from "./routing";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    AuthenticationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(authenticationRoutes),
    MatCardModule
  ]
})
export class AuthenticationModule { }
