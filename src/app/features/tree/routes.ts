import {Routes} from "@angular/router";
import {TreeComponent} from "./tree.component";

export const treeRoutes: Routes = [
  {
    path: '',
    component: TreeComponent,
    children: [
      {
        path: 'map',
        loadChildren: async () =>
          (await import('./features/map')).mapRoutes,
      },
      {
        path: '',
        loadChildren: async () =>
          (await import('./features/my-tree')).myTreeRoutes
      }
    ]
  }
]
