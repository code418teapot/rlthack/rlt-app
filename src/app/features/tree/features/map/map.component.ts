import {Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef} from '@angular/core';
import { regions } from './utils/regions.map';
import {NgForOf} from "@angular/common";
import {MatTooltipModule} from "@angular/material/tooltip";
import {TreesDataAccessService} from "../../../../data-access";
import {MapRegion} from "./utils/map-region.type";

@Component({
  standalone: true,
  selector: 'pyteka-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgForOf,
    MatTooltipModule
  ]
})
export class MapComponent implements OnInit {
  regions: (MapRegion & { count: number })[] = [];

  constructor(
    private _treeService: TreesDataAccessService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {
    this._treeService.getTreeRegionData()
      .subscribe(treeData => {
        this.regions = regions.map(region => ({
          ...region,
          count: treeData[region.id] ?? 0
        }));

        console.log(this.regions);

        this._changeDetectorRef.detectChanges();
      })
  }

  getTooltip(region: MapRegion & { count: number }) {
    return `${region.displayName} - Посажено ${region.count} деревьев`
  }
}
