export interface MapRegion {
  id: string;
  displayName: string;
  svgPath: string;
}
