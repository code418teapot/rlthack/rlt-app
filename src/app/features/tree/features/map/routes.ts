import {Routes} from "@angular/router";
import {MapComponent} from "./map.component";

export const mapRoutes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: MapComponent
  }
]
