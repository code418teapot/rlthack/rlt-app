import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatButtonModule} from "@angular/material/button";
import {bonusCountSelector, userProfileSelector} from "../../../user/store/selectors";
import {Store} from "@ngrx/store";
import {VariableDirective} from "../../../../shared/directives";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {RouterLink} from "@angular/router";
import {ApplicationRoutes} from "../../../../core/routing";
import {CompanyStore} from "../../../company/store";
import {MatIconModule} from "@angular/material/icon";
import {BasicSvgIcon} from "../../../../shared/svg-icons/svg-icons.enum";

const TREE_IMAGE_PATH = 'assets/tree-progress'

@Component({
  selector: 'app-my-tree',
  standalone: true,
  imports: [CommonModule, NgOptimizedImage, MatButtonModule, VariableDirective, MatDialogModule, RouterLink, MatIconModule],
  templateUrl: './my-tree.component.html',
  styleUrls: ['./my-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    CompanyStore
  ]
})
export class MyTreeComponent implements OnInit {
  readonly userProfile$ = this._globalStore.select(userProfileSelector);
  readonly bonusCount$ = this._globalStore.select(bonusCountSelector);
  readonly treesCount$ = this._companyStore.treesCount$

  treeImage: string = `${TREE_IMAGE_PATH}/tree-1.svg`

  readonly treePrice = 50;

  constructor(
    private _globalStore: Store,
    private _companyStore: CompanyStore,
    private _cd: ChangeDetectorRef,
    private _dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.userProfile$.subscribe({
      next: value => this.getTreePicture(value.bonusCount)
    })
  }

  getTreePicture(bonuses: number | undefined) {
    var treeProgress = 1;
    if (bonuses) {
      if (bonuses >= 50) {
        treeProgress = 3
      } else if (bonuses >= 30) {
        treeProgress = 2
      } else if (bonuses >= 0) {
        treeProgress = 1
      } else {
        treeProgress = 1
      }
      this.treeImage = `${TREE_IMAGE_PATH}/tree-${treeProgress}.svg`
      this._cd.markForCheck()
    }
  }

  plantTree() {
    this._companyStore.plantTree()
  }

  getRemainingBonuses(bonusCount: number) {
    return Math.max(0, this.treePrice - bonusCount);
  }

  protected readonly ApplicationRoutes = ApplicationRoutes;
  protected readonly BasicSvgIcon = BasicSvgIcon;
}
