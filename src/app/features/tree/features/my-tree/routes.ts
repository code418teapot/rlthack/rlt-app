import {Routes} from "@angular/router";
import {MyTreeComponent} from "./my-tree.component";

export const myTreeRoutes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: MyTreeComponent,
  }
]
