import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterLink, RouterOutlet} from "@angular/router";
import {MatTabsModule} from "@angular/material/tabs";
import {ApplicationRoutes} from "../../core/routing";

@Component({
  selector: 'app-tree',
  standalone: true,
  imports: [CommonModule, RouterOutlet, MatTabsModule, RouterLink],
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeComponent {
  activeLink: string = 'tree';

  protected readonly ApplicationRoutes = ApplicationRoutes;
}
