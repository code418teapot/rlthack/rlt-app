export enum UserProfileActionTypes {
  GetUserProfile = '[User] Get User Profile',
  GetUserProfileSuccess = '[User] Get User Profile Success',
  GetUserProfileError = '[User] Get User Profile Error',
  CreateAchievement = '[User] Create Achievement',
  CreateAchievementSuccess = '[User] Create Achievement Success',
  EnableEdx = '[User] Enable EDX'
}
