import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {catchError, delay, map, switchMap, tap} from "rxjs/operators";
import {combineLatest, EMPTY, of, withLatestFrom} from "rxjs";
import {Store} from "@ngrx/store";
import {CompanyDataAccessService, UsersDataAccessService} from "../../../data-access";
import {
  createAchievement,
  createAchievementSuccess,
  enableEdx,
  getUserProfile,
  getUserProfileFailure,
  getUserProfileSuccess
} from "./actions";
import {AchievementsDataAccessService} from "../../../data-access/services/achievements.service";
import {NotificationService} from "../../../core/notifications";
import {ApplicationRoutes} from "../../../core/routing";
import {FrontendAchievements} from "../../achievements";
import {isEdxEnabledSelector} from "./selectors";

export const saleBonusCount = 30;

@Injectable()
export class UserProfileEffects {
  getUserProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(getUserProfile),
      switchMap(() =>
        combineLatest([
          this._userDataAccessService.getUserProfile(),
          this._companyDataAccessService.getMyCompany(),
        ])
          .pipe(
            map(([userDto, companyDto]) => getUserProfileSuccess({
              ...userDto,
              bonusCount: companyDto?.bonusCount || 0
            })),
            catchError(() => of(getUserProfileFailure()))
          )
      )
    )
  );

  getUserProfileSuccessToSendSaleNotification$ = createEffect(() =>
      this._actions$.pipe(
        ofType(getUserProfileSuccess),
        delay(7000),
        withLatestFrom(this._globalStore.select(isEdxEnabledSelector)),
        tap(([userProfile, isEdxEnabled]) => {
          if (userProfile.bonusCount >= saleBonusCount) {
            this._notificationService.showSuccess({
              title: 'Доступна скидка',
              message: 'У вас уже накопилось достаточно баллов на скидку',
              urlTitle: 'Подробнее',
              urlLink: ['/', !isEdxEnabled ? ApplicationRoutes.Sections : ApplicationRoutes.Services]
            }, 10)
          }
        })
      )
    , {dispatch: false});

  createAchievement$ = createEffect(() =>
    this._actions$.pipe(
      ofType(createAchievement),
      switchMap(({achievement}) =>
        this._achievementsService.createAchievement(achievement).pipe(
          map(() => createAchievementSuccess()),
          catchError(() => EMPTY)
        )
      )
    )
  );

  createAchievementSuccess$ = createEffect(() =>
    this._actions$.pipe(
      ofType(createAchievementSuccess),
      switchMap(() => of(getUserProfile()))
    )
  );

  edxEnabledShouldCreateAchievement = createEffect(() =>
    this._actions$.pipe(
      ofType(enableEdx),
      switchMap(() => of(createAchievement({
        achievement: FrontendAchievements.BuyEdx
      }))),
      tap(() => {
        this._notificationService.showSuccess({
          title: 'Новое достижение',
          message: 'Вам начислено 60 баллов за достижение “Никакой бумаги”',
          urlTitle: 'Подробнее',
          urlLink: ['/', ApplicationRoutes.Company]
        }, 10)
      })
    )
  );

  constructor(
    private _globalStore: Store,
    private _actions$: Actions,
    private _userDataAccessService: UsersDataAccessService,
    private _companyDataAccessService: CompanyDataAccessService,
    private _achievementsService: AchievementsDataAccessService,
    private _notificationService: NotificationService,
  ) {
  }
}
