import {createReducer, on} from "@ngrx/store";
import {UserProfileState} from "./types";
import {enableEdx, getUserProfile, getUserProfileFailure, getUserProfileSuccess} from "./actions";

const initialState: UserProfileState = {
  isLoading: false,
  isEdxEnabled: false
};

export const userProfileReducer = createReducer<UserProfileState>(
  initialState,
  on(getUserProfile, (state) => ({...state, isLoading: true})),
  on(getUserProfileSuccess, (state, profile) => {
    return {
      ...state,
      isLoading: false,
      user: {
        id: profile.id,
        firstName: profile.firstName,
        lastName: profile.lastName,
        middleName: profile.middleName,
        inn: profile.inn,
        inviteUid: profile.inviteUid,
        invitedBy: profile.invitedBy
      },
      bonusCount: profile.bonusCount
    };
  }),
  on(getUserProfileFailure, () => ({...initialState})),
  on(enableEdx, (state) => ({ ...state, isEdxEnabled: true })),
);
