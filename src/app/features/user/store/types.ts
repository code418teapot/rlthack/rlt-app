export const featureName = 'profile';

export interface UserProfile {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
  inn: string;
  bonusCount: number;
  inviteUid: string;
  invitedBy?: number;
}

export interface UserProfileState {
  isLoading: boolean;
  user?: {
    id: number;
    firstName: string;
    lastName: string;
    middleName: string;
    inn: string;
    inviteUid: string;
    invitedBy?: number;
  },
  bonusCount?: number;
  isEdxEnabled: boolean;
  telegramId?: number;
}

