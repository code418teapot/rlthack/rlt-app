import {createAction, props} from '@ngrx/store';
import {UserProfileActionTypes} from './action-types';
import {UserProfile} from './types';
import {FrontendAchievements} from "../../achievements";

export const getUserProfile = createAction(UserProfileActionTypes.GetUserProfile);
export const getUserProfileSuccess = createAction(
  UserProfileActionTypes.GetUserProfileSuccess,
  props<UserProfile>()
);
export const getUserProfileFailure = createAction(UserProfileActionTypes.GetUserProfileError);

export const createAchievement = createAction(
  UserProfileActionTypes.CreateAchievement,
  props<{
    achievement: FrontendAchievements
  }>()
);

export const createAchievementSuccess = createAction(
  UserProfileActionTypes.CreateAchievementSuccess
);

export const enableEdx = createAction(UserProfileActionTypes.EnableEdx);
