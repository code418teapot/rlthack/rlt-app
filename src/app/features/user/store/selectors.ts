import {createFeatureSelector, createSelector} from '@ngrx/store';
import {featureName, UserProfileState} from './types';

export const userProfileSelector =
  createFeatureSelector<UserProfileState>(featureName);

export const userInfoSelector = createSelector(
  userProfileSelector,
  (state) => state.user
);

export const bonusCountSelector = createSelector(
  userProfileSelector,
  (state) => state.bonusCount
);

export const isEdxEnabledSelector = createSelector(
  userProfileSelector,
  (state) => state.isEdxEnabled
);
