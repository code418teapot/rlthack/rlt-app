import {EnvironmentProviders, Provider} from "@angular/core";
import {provideEffects} from "@ngrx/effects";
import {UserProfileEffects} from "./store/effects";
import {provideState} from "@ngrx/store";
import {featureName} from "./store/types";
import {userProfileReducer} from "./store/reducers";

export function provideUserProfile(): (Provider | EnvironmentProviders)[] {
  return [
    provideState(featureName, userProfileReducer),
    provideEffects(UserProfileEffects)
  ]
}
