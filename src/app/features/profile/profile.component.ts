import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Store} from "@ngrx/store";
import {userProfileSelector} from "../user/store/selectors";
import {isNil} from "lodash";
import {VariableDirective} from "../../shared/directives";
import {AuthenticationRoutes} from "../authentication/routing/routes.enum";
import {ApplicationRoutes} from "../../core/routing";
import {UsersDataAccessService} from "../../data-access";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [CommonModule, VariableDirective, MatInputModule, MatButtonModule, MatIconModule],
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent {

  readonly userProfile$ = this._globalStore.select(userProfileSelector)
  referalUrl: string | null = null

  telegramCode: string = '';

  constructor(
    private _globalStore: Store,
    private _userDataAccessService: UsersDataAccessService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    this.userProfile$.subscribe({
      next: value => {
        if (value.user) {
          this.generateReferalUrl(value.user?.inviteUid)
        }
      }
    })
  }

  ngOnInit() {
    this._userDataAccessService.getUserTelegramCode()
      .subscribe(code => {
        this.telegramCode = code;
        this._changeDetectorRef.detectChanges();
      })
  }

  generateReferalUrl(inviteUid: string) {
    this.referalUrl = `${window.location.origin}/${ApplicationRoutes.Authentication}/${AuthenticationRoutes.CreateProfile}?inviteUid=${inviteUid}`
  }

  async copyReferalCode(){
    await navigator.clipboard.writeText(this.referalUrl ?? '');
  }

  async copyTelegramCode(){
    await navigator.clipboard.writeText(this.telegramCode);
  }

  protected readonly isNil = isNil;
}
