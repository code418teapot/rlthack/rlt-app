export * from './routes';

export enum FrontendAchievements {
  GettingStarted = 'GETTING_STARTED',
  BuyEdx = 'BUY_EDO',
  SignDocEdo = 'SIGN_DOC_EDO'
}
