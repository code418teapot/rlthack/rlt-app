import {Routes} from '@angular/router';
import {LayoutComponent} from "./core/layout";
import {ApplicationRoutes} from "./core/routing";
import {AlreadyAuthenticatedGuard, AuthenticationGuard} from "./features/authentication/core";
import {provideUserProfile} from "./features/user/users.providers";

export const routes: Routes = [
  {
    path: ApplicationRoutes.Authentication,
    canActivate: [AlreadyAuthenticatedGuard],
    loadChildren: async () =>
      (await import('./features/authentication')).AuthenticationModule,
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthenticationGuard],
    providers: [
      provideUserProfile(),
    ],
    children: [
      {
        path: ApplicationRoutes.Sections,
        loadChildren: async () =>
          (await import('./features/services')).servicesRoutes,
      },
      {
        path: ApplicationRoutes.Company,
        loadChildren: async () =>
          (await import('./features/company')).companyRoutes,
      },
      {
        path: ApplicationRoutes.Achievements,
        loadChildren: async () =>
          (await import('./features/achievements')).achievementsRoutes,
      },
      {
        path: ApplicationRoutes.Trees,
        loadChildren: async () =>
          (await import('./features/tree')).treeRoutes,
      },
      {
        path: ApplicationRoutes.EDX,
        loadChildren: async () =>
          (await import('./features/edx')).edxRoutes,
      },
      {
        path: ApplicationRoutes.Services,
        loadChildren: async () =>
          (await import('./features/company-services')).companyServiceRoutes,
      },
      {
        path: ApplicationRoutes.Profile,
        loadChildren: async () =>
          (await import('./features/profile')).profileRoutes,
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: ApplicationRoutes.Company
      }
    ]
  },
];
